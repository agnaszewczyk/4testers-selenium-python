import pytest
from selenium.webdriver import Chrome

from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_project_management_page import ArenaProjectManagementPage
from pages.arena.arena_result_page import ProjectResultPage

from utils.random_message import generate_random_text

@pytest.fixture
def browser():
    driver = Chrome()
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    arena_home_page = ArenaHomePage(driver)
    arena_home_page.click_tools_icon()
    yield driver
    driver.quit()



def test_add_new_project_and_check(browser):
    arena_project_management_page = ArenaProjectManagementPage(browser)
    arena_project_management_page.add_new_project()
    name_of_the_project = generate_random_text(10)
    arena_project_management_page.add_info_to_project(name_of_the_project)
    arena_project_management_page.save_project()
    arena_result_page = ProjectResultPage(browser)
    arena_result_page.go_to_projects()
    arena_project_management_page.search_for_project(name_of_the_project)
    arena_result_page.verify_created_project(name_of_the_project)



